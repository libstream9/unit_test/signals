#ifndef STREAM9_SIGNALS_TEST_SRC_NAMESPACE_HPP
#define STREAM9_SIGNALS_TEST_SRC_NAMESPACE_HPP

namespace stream9::signals {}
namespace stream9::strings {}

namespace testing {

namespace sig { using namespace stream9::signals; }
namespace str { using namespace stream9::strings; }

} // namespace testing

#endif // STREAM9_SIGNALS_TEST_SRC_NAMESPACE_HPP

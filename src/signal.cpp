#include <stream9/signals/signal.hpp>

#include "namespace.hpp"

#include <concepts>

#include <boost/test/unit_test.hpp>

#include <stream9/strings/stream.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(signal_)

BOOST_AUTO_TEST_CASE(concepts_)
{
    static_assert(std::semiregular<sig::signal<void()>>);
}

BOOST_AUTO_TEST_CASE(construction_)
{
    sig::signal<void()> s1;
}

BOOST_AUTO_TEST_CASE(one_connection_)
{
    sig::signal<void()> s1;

    std::string s;
    using str::operator<<;

    auto c1 = s1.connect([&]{ s << "hello, world"; });

    s1.emit();

    BOOST_TEST(s == "hello, world");
}

BOOST_AUTO_TEST_CASE(two_connection_)
{
    sig::signal<void()> s1;

    std::string s;
    using str::operator<<;

    auto c1 = s1.connect([&]{ s << "hello"; });
    auto c2 = s1.connect([&]{ s << ", world"; });

    s1.emit();

    BOOST_TEST(s == "hello, world");
}

BOOST_AUTO_TEST_SUITE_END() // signal_

} // namespace testing

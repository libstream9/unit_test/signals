#include <stream9/signals/signal.hpp>

#include "namespace.hpp"

#include <ranges>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(result_range_)

BOOST_AUTO_TEST_CASE(concept_)
{
    sig::signal<int()> sig1;

    using R = decltype(sig1.emit());
    static_assert(std::ranges::bidirectional_range<R>);
}

BOOST_AUTO_TEST_CASE(no_args_)
{
    sig::signal<int()> sig1;

    auto c1 = sig1.connect([]{ return 1; });
    auto c2 = sig1.connect([]{ return 2; });
    auto c3 = sig1.connect([]{ return 3; });

    int result = 0;

    for (auto r: sig1.emit()) {
        result += r;
    }

    BOOST_TEST(result == 6);
}

BOOST_AUTO_TEST_CASE(one_lvalue_arg_1_)
{
    using R = size_t;
    using A = std::string const&;

    sig::signal<R(A)> sig1;

    auto slot = [](auto&& s){ return s.size(); };

    auto c1 = sig1.connect(slot);
    auto c2 = sig1.connect(slot);

    size_t result = 0;
    std::string a1 = "foo";

    for (auto r: sig1.emit(a1)) {
        result += r;
    }

    BOOST_TEST(result == 6);
}

BOOST_AUTO_TEST_CASE(one_lvalue_arg_2_)
{
    using R = size_t;
    using A = std::string;

    sig::signal<R(A)> sig1;

    auto slot = [](auto&& s){ return s.size(); };

    auto c1 = sig1.connect(slot);
    auto c2 = sig1.connect(slot);

    size_t result = 0;
    std::string a1 = "foo";

    for (auto r: sig1.emit(a1)) {
        result += r;
    }

    BOOST_TEST(result == 6);
}

BOOST_AUTO_TEST_CASE(two_lvalue_args_)
{
    using R = size_t;
    using A = std::string const&;

    sig::signal<R(A, A)> sig1;

    auto slot = [](auto&& s1, auto&& s2){ return s1.size() + s2.size(); };

    auto c1 = sig1.connect(slot);
    auto c2 = sig1.connect(slot);

    size_t result = 0;
    std::string a1 = "foo";

    for (auto r: sig1.emit(a1, a1)) {
        result += r;
    }

    BOOST_TEST(result == 12);
}

BOOST_AUTO_TEST_CASE(one_rvalue_arg_1_)
{
    using R = size_t;
    using A = std::string const&;

    sig::signal<R(A)> sig1;

    auto slot = [](auto&& s){ return s.size(); };

    auto c1 = sig1.connect(slot);
    auto c2 = sig1.connect(slot);

    size_t result = 0;
    std::string a1 = "foo";

    for (auto r: sig1.emit(std::move(a1))) {
        result += r;
    }

    BOOST_TEST(result == 6);
}

BOOST_AUTO_TEST_CASE(one_rvalue_arg_2_)
{
    using R = size_t;
    using A = std::string;

    sig::signal<R(A)> sig1;

    auto slot = [](auto&& s){ return s.size(); };

    auto c1 = sig1.connect(slot);
    auto c2 = sig1.connect(slot);

    size_t result = 0;
    std::string a1 = "foo";

    for (auto r: sig1.emit(std::move(a1))) {
        result += r;
    }

    BOOST_TEST(result == 6);
}

BOOST_AUTO_TEST_CASE(two_rvalue_arg_)
{
    using R = size_t;
    using A = std::string const&;

    sig::signal<R(A, A)> sig1;

    auto slot = [](auto&& s1, auto&& s2){ return s1.size() + s2.size(); };

    auto c1 = sig1.connect(slot);
    auto c2 = sig1.connect(slot);

    size_t result = 0;
    std::string a1 = "foo";
    std::string a2 = "xyzzy";

    for (auto r: sig1.emit(std::move(a1), std::move(a2))) {
        result += r;
    }

    BOOST_TEST(result == 16);
}

BOOST_AUTO_TEST_CASE(blocked_signal_)
{
    sig::signal<int()> sig1;

    auto c1 = sig1.connect([]{ return 1; });
    auto c2 = sig1.connect([]{ return 2; });
    auto c3 = sig1.connect([]{ return 3; });

    int result = 0;

    sig1.set_block(true);

    for (auto r: sig1.emit()) {
        result += r;
    }

    BOOST_TEST(result == 0);
}

BOOST_AUTO_TEST_CASE(blocked_slot_)
{
    sig::signal<int()> sig1;

    auto c1 = sig1.connect([]{ return 1; });
    auto c2 = sig1.connect([]{ return 2; });
    auto c3 = sig1.connect([]{ return 3; });

    int result = 0;

    c2.set_block(true);

    for (auto r: sig1.emit()) {
        result += r;
    }

    BOOST_TEST(result == 4);
}

BOOST_AUTO_TEST_SUITE_END() // result_range_

} // namespace testing

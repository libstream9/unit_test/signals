#include <stream9/signals/signal_block.hpp>
#include <stream9/signals/connection_block.hpp>

#include <stream9/signals/signal.hpp>

#include "namespace.hpp"

#include <concepts>

#include <boost/test/unit_test.hpp>

#include <stream9/strings/stream.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(block_)

BOOST_AUTO_TEST_CASE(concept_)
{
    static_assert(std::movable<sig::signal_block>);
    static_assert(std::movable<sig::connection_block>);
}

BOOST_AUTO_TEST_CASE(block_signal_)
{
    sig::signal<void()> s1;

    std::string s;
    using str::operator<<;

    auto c1 = s1.connect([&]{ s << "hello, world"; });

    BOOST_TEST(!s1.is_blocked());
    {
        auto b = s1.block();

        BOOST_TEST(s1.is_blocked());

        s1.emit();

        BOOST_TEST(s.empty());
    }
    BOOST_TEST(!s1.is_blocked());

    s1.emit();

    BOOST_TEST(s == "hello, world");
}

BOOST_AUTO_TEST_CASE(block_slot_)
{
    sig::signal<void()> s1;

    std::string s;
    using str::operator<<;

    auto c1 = s1.connect([&]{ s << "hello"; });
    auto c2 = s1.connect([&]{ s << ", world"; });

    BOOST_TEST(!c1.is_blocked());
    BOOST_TEST(!c2.is_blocked());
    {
        auto b1 = c2.block();
        BOOST_TEST(!c1.is_blocked());
        BOOST_TEST(c2.is_blocked());

        s1.emit();

        BOOST_TEST(s == "hello");
    }
    BOOST_TEST(!c1.is_blocked());
    BOOST_TEST(!c2.is_blocked());

    s.clear();
    s1.emit();

    BOOST_TEST(s == "hello, world");
}

BOOST_AUTO_TEST_SUITE_END() // block_

} // namespace testing
